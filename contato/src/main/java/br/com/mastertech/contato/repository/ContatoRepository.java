package br.com.mastertech.contato.repository;

import br.com.mastertech.contato.model.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ContatoRepository extends CrudRepository<Contato, Long> {
    Optional<Contato> findByName(String name);
    List<Contato> findAllByName(String name);
}
