package br.com.mastertech.contato.controller;

import br.com.mastertech.contato.model.Contato;
import br.com.mastertech.contato.security.UsuarioOauth;
import br.com.mastertech.contato.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
public class ContatoController {

    @Autowired
    ContatoService contatoService;
    @PostMapping("/contato")
    public Contato create(@RequestBody Contato contato, @AuthenticationPrincipal UsuarioOauth usuarioOauth){
        contato.setName(usuarioOauth.getName());
        contatoService.create(contato);
        return contato;
    }
    @GetMapping(("/contatos"))
    public List<Contato> contatos(@AuthenticationPrincipal UsuarioOauth usuarioOauth){
        return contatoService.findAllByName(usuarioOauth.getName());
    }
}
