package br.com.mastertech.contato.service;

import br.com.mastertech.contato.model.Contato;
import br.com.mastertech.contato.repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    public Contato create(Contato contato){
        contatoRepository.save(contato);
        return contato;
    }

    public Optional<Contato> findByName(String name){

        return contatoRepository.findByName(name);
    }

    public List<Contato> findAllByName(String name){
        return contatoRepository.findAllByName(name);
    }
}
